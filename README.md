# Bacmman object classifier

Deep-learning classifier for objects segmented in Bacmman.

## Setup
- Install the package with the command `pip install git+https://gitlab.com/MEKlab/bacmman-object-classifier.git`
- Download the Jupyter notebooks from the [Notebooks folder](https://gitlab.com/MEKlab/bacmman-object-classifier/-/tree/main/Notebooks?ref_type=heads)

## Training

### Generating training data
- Choose a dataset to use as training data, which contains at least 20-30 images
- Segment the object of interest in BACMMAN
- In the Data Browsing tab, create one selection for each of your object classes
- Manually add objects to their corresponding selections
    - It is important that all the cells in an image belong to a selection (no un-classified cells)
- Export selections
    - Select all the selections you have created (with shift+click)
    - Go to `Run` -> `Extract Selected Selections`
- Generate a hdf5 file with fluorescence images and segmentation masks
    - In the Data Browsing tab, select all labelled images, then create a "Viewfield" selection
    - In the Home tab, right-click in the `Tasks to Execute panel`
    - Select `Extract Dataset` -> `Add new Dataset Extraction Task to List`
    - Input extraction parameters as shown on the screenshot below

![Screenshot of the Bacmman dataset extraction menu with relevant options shown](./Images/BACMMAN_dataset_extraction.jpg)

- Open the `Make_training_data` notebook, and run all cells (update paths and names where necessary)

### Training
- Open the `Training` notebook
- Run cells and update options when necessary
    - Under `Create Iterators`, you can change the tile size (choose one bigger than your objects), zoom_range and aspect_ratio_range (set to [1, 1] to not use) and elastic_deform
    - Under `Initialise Unet Model`, input the category weights that were recommended at the end of the `Make_training_data` notebook
    - Under `Train model`, adjust the number of epochs (start with 1 to check that the training does not crash, then increase to e.g. 500)

## Assessing accuracy and saving a model
- Open the `Accuracy_per_cell` notebook
- Under `model_file`, enter the name of the model weights (in .h5 format) generated during the training
- The notebook will calculate the percentage of correctly classified objects using a part of the dataset not used in training
- The cell under `Save the Model` allows you to save the model with the weights in a Bacmman-readable format

## Applying the model in BACMMAN
- In your Bacmman configuration, add a `DLObjectClassifier` measurement
- Under `DLEngine` choose `TF2 engine`, and under `Model File`, input the path to the model saved in the `Accuracy_per_cell` notebook
