import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="objectClassifier",
    version="0.0.1",
    author="Daniel Thedie",
    author_email="daniel.thedie@ed.ac.uk",
    description="Deep-learning model for classification of Bacmman objects",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/MEKlab/bacmman-object-classifier",
    packages=setuptools.find_packages(),
    python_requires='>=3.9',
    install_requires=['numpy', 'pandas', 'matplotlib', 'h5py', 'talissman', 'tensorflow', 'IPython>=8.13.2']
)
