from talissman.training.data_augmentation import get_center_scale_range


def get_center_scale(dataset_path, raw_keyword: str = 'raw'):
    center_range, scale_range = get_center_scale_range(dataset_path, raw_feature_name=raw_keyword, fluorescence=True,
                                                       fluo_centile_range=[95, 95], fluo_centile_extent=0)
    center = center_range[0]
    scale = scale_range[0]
    return center, scale
