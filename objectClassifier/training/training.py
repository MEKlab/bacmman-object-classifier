import numpy as np
import edt
from talissman.training.data_augmentation import get_normalization_fun
from dataset_iterator import MultiChannelIterator
from dataset_iterator.tile_utils import extract_tile_random_zoom_function
import tensorflow as tf
from tensorflow.keras.losses import SparseCategoricalCrossentropy
from tensorflow.keras.optimizers import Adam

from ..model.unet_pix_mclass import get_unet
from .weighted_loss import weighted_loss_by_category


def get_iterators(dataset,
                  center_range: list,
                  scale_range: list,
                  val_keyword: str = 'eval', train_keyword: str = 'train',
                  raw_keyword: str = 'raw', label_keyword: str = 'regionLabels', cells_keyword='cells',
                  tile_size: int = 128,
                  zoom_range: list = [0.95, 1.05],
                  aspect_ratio_range: list = [0.95, 1.05],
                  elastic_deform: int = 0,
                  use_edm: bool = True,
                  batch_size: int = 5):

    scale_fun = [[get_normalization_fun(center_range, scale_range), None, None, None]]
    tile_function = extract_tile_random_zoom_function(tile_shape=(tile_size, tile_size),
                                                      zoom_range=zoom_range, aspect_ratio_range=aspect_ratio_range,
                                                      perform_augmentation=True, random_stride=True)

    if use_edm:
        def cell_label_fun(x):
            return np.stack([edt.edt(x[i, ..., 0], black_border=True)[..., np.newaxis] for i in range(x.shape[0])])
    else:
        def cell_label_fun(x):
            return np.where(x > 0, 1, x)  # Use binary mask

    # Iterator parameters
    params = dict(dataset=dataset,
                  channel_keywords=[raw_keyword, label_keyword, label_keyword, cells_keyword],
                  group_scaling=scale_fun,
                  input_channels=[0, 1],
                  mask_channels=[1, 2, 3],
                  output_channels=[2, 3],
                  extract_tile_function=tile_function,
                  elasticdeform_parameters={'sigma': elastic_deform},
                  channels_postprocessing_function=_channels_postprocessing(lambda x: x,
                                                                            cell_label_fun,
                                                                            lambda x: x,
                                                                            lambda x: x),
                  perform_data_augmentation=True,
                  batch_size=batch_size,
                  shuffle=True
                  )

    train_it = MultiChannelIterator(group_keyword=train_keyword, **params)
    test_it = MultiChannelIterator(group_keyword=val_keyword, **params)

    return train_it, test_it


def get_model(n_classes: int = 2, n_fluo: int = 1, learning_rate: float = 0.0004, weights: list = []):
    assert len(weights) == n_classes, 'Weights given are inconsistent with number of categories'
    model = get_unet(n_classes, n_input_channels=n_fluo)
    optimizer = Adam(learning_rate=learning_rate)
    loss_function = weighted_loss_by_category(SparseCategoricalCrossentropy(from_logits=False,
                                                                            reduction=tf.keras.losses.Reduction.NONE
                                                                            ),
                                              weights, axis=-1, sparse=True, dtype='float32')
    model.compile(loss=loss_function, optimizer=optimizer, metrics=['SparseCategoricalAccuracy'])
    return model


def _channels_postprocessing(*args):
    def fun(batch_by_channel):
        for j, _ in enumerate(args):
            batch_by_channel[j] = args[j](batch_by_channel[j])
    return fun


def get_edm(mask):
    edm = edt.edt(mask[0, ], black_border=True)
    edm = edm[np.newaxis, ...]
    return edm
