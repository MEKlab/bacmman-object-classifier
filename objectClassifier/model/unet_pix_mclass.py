import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import (Concatenate, Conv2D, Conv2DTranspose,
                                     Input, MaxPool2D)


class CustomModel(keras.Model):
    def train_step(self, data):
        # Unpack the data. Its structure depends on your model and
        # on what you pass to `fit()`.
        x, y = data
        label_rank, label_size = _get_label_rank_and_size(y[1])

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True)  # Forward pass
            y_pred = _get_mean_by_object(y_pred, label_rank, label_size)
            # Compute the loss value
            # (the loss function is configured in `compile()`)
            loss = self.compiled_loss(y[0], y_pred, regularization_losses=self.losses)

        # Compute gradients
        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        # Update metrics (includes the metric that tracks the loss)
        self.compiled_metrics.update_state(y[0], y_pred)
        # Return a dict mapping metric names to current value
        return {m.name: m.result() for m in self.metrics}

    def test_step(self, data):
        # Unpack the data
        x, y = data
        label_rank, label_size = _get_label_rank_and_size(y[1])

        # Compute predictions
        y_pred = self(x, training=False)
        y_pred = _get_mean_by_object(y_pred, label_rank, label_size)
        # Updates the metrics tracking the loss
        self.compiled_loss(y[0], y_pred, regularization_losses=self.losses)
        # Update the metrics.
        self.compiled_metrics.update_state(y[0], y_pred)
        # Return a dict mapping metric names to current value.
        # Note that it will include the loss (tracked in self.metrics).
        return {m.name: m.result() for m in self.metrics}


def _get_mean_by_object(data, label_rank, label_size):
    mean = tf.reduce_sum(label_rank * tf.expand_dims(data, -1),
                         axis=[1, 2], keepdims=True) / label_size  # shape = (batch, y=1, x=1, c, n_label_max)
    return tf.reduce_sum(mean * label_rank, axis=-1)  # shape = (batch, y, x, c)


def _get_label_rank_and_size(labels):
    labels = tf.cast(labels, dtype=tf.int32)
    label_rank = tf.one_hot(labels-1, tf.math.reduce_max(labels),
                            dtype=tf.float32)  # shape = (batch, y, x, c, n_label_max)
    label_size = tf.reduce_sum(label_rank, axis=[1, 2],
                               keepdims=True)  # shape = (batch, y=1, x=1, c, n_label_max)
    label_size = tf.where(label_size == 0, 1., label_size)  # avoid nans when dividing by size
    return label_rank, label_size


ENCODER_SETTINGS = [
    [  # l1 = 128 -> 64
        {"filters": 128},
        {"filters": 128, "downscale": 2, "maxpool": True}
    ],
    [  # l2 64 -> 32
        {"filters": 128},
        {"filters": 128, "downscale": 2, "maxpool": True}
    ],
    [  # l3: 32->16
        {"filters": 128, "kernel_size": 3},
        {"filters": 128},
        {"filters": 128, "downscale": 2, "maxpool": True},
    ],
    [  # l4: 16 -> 8
        {"filters": 256, "kernel_size": 3},
        {"filters": 256},
        {"filters": 256, "downscale": 2, "maxpool": True}
    ]
]
FEATURE_SETTINGS = [
    {"filters": 256, "kernel_size": 3},
    {"filters": 256},
]

DECODER_SETTINGS = [
    {"filters": 128},
    {"filters": 128},
    {"filters": 128},
    {"filters": 256}
]


def get_unet(n_classes, n_input_channels=1, encoder_settings=ENCODER_SETTINGS,
             feature_settings=FEATURE_SETTINGS, decoder_settings=DECODER_SETTINGS,
             name="unet"):

    assert len(encoder_settings) == len(decoder_settings), "encoder and decoder must have same depth"
    input_fluo = Input(shape=(None, None, n_input_channels), name=name + "_input_fluo")
    input_edm = Input(shape=(None, None, 1), name=name + "_input_edm")
    input = Concatenate(axis=-1, name=name + "_input")([input_edm, input_fluo])

    residuals = []
    downsample_size = []
    for d, parameters in enumerate(encoder_settings):
        down, residual, down_size = downsampling_block(input if d == 0 else down, parameters, d, name)
        residuals.append(residual)
        downsample_size.append(down_size)
    up = downsampling_block(down, feature_settings, len(encoder_settings), name)
    for d in range(len(decoder_settings)-1, -1, -1):
        up = upsampling_block(up, residuals[d], decoder_settings[d], downsample_size[d], d, name)

    output = Conv2D(filters=n_classes, kernel_size=1, padding='same', activation='softmax', name=name+"_output")(up)
    return CustomModel([input_fluo, input_edm], output, name=name)


def downsampling_block(input, parameters, l_idx, name):
    convolutions = [input]
    for i, params in enumerate(parameters):
        downsample = params.get("downscale", 1)
        maxpool = params.get("maxpool", False)
        assert downsample == 1 or i == len(parameters)-1, "downscale > 1 must be last convolution"
        conv = Conv2D(filters=params["filters"], kernel_size=params.get("kernel_size", 3), padding='same',
                      activation=params.get("activation", "relu"), strides=1 if maxpool else downsample,
                      name=f"{name}/encoder/{l_idx}/conv_{i}")(convolutions[-1])
        convolutions.append(conv)
        if downsample > 1 and maxpool:
            mp = MaxPool2D(pool_size=downsample, name=f"{name}/encoder/{l_idx}/mp_{i}")(convolutions[-1])
            convolutions.append(mp)
    downsample = parameters[-1].get("downscale", 1)
    if downsample > 1:
        return convolutions[-1], convolutions[-2], downsample
    else:
        return convolutions[-1]


def upsampling_block(input, residual, params, stride, l_idx, name):
    up = Conv2DTranspose(params["filters"], kernel_size=params.get("up_kernel_size", 4), strides=stride,
                         padding='same', activation=params.get("activation", "relu"),
                         name=f"{name}/decoder/{l_idx}/upConv")(input)
    if residual is not None:
        concat = Concatenate(axis=-1, name=f"{name}/decoder/{l_idx}/concat")([residual, up])
    else:
        concat = up
    conv = Conv2D(filters=params["filters"], kernel_size=params.get("kernel_size", 3), padding='same',
                  activation=params.get("activation", "relu"), name=f"{name}/decoder/{l_idx}/conv1")(concat)
    return Conv2D(filters=params["filters"] if l_idx == 0 else params["filters"]//2,
                  kernel_size=params.get("kernel_size", 3), padding='same',
                  activation=params.get("activation", "relu"), name=f"{name}/decoder/{l_idx}/conv2")(conv)
