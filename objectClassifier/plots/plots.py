import matplotlib.pyplot as plt
import numpy as np


def plot_images(batch_x, batch_y, n=3):

    batch_fluo, batch_edm = batch_x
    batch_y1, _ = batch_y

    plt.figure(figsize=(9, n*3))
    for i in range(0, n*3, 3):
        plt.subplot(n, 3, i+1)
        plt.imshow(batch_fluo[i, :, :, 0], cmap="gray")
        plt.axis("off")
        if i == 0:
            plt.title('Fluorescence')

        plt.subplot(n, 3, i+2)
        plt.imshow(batch_edm[i, :, :, 0], cmap="gray")
        plt.axis("off")
        if i == 0:
            plt.title('EDM')

        plt.subplot(n, 3, i+3)
        plt.imshow(batch_y1[i, :, :, 0], cmap="nipy_spectral", vmin=0, vmax=3)
        plt.axis("off")
        if i == 0:
            plt.title('Ground-truth labels')


def plot_training_history(history):

    # plot accuracy
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(history.history['sparse_categorical_accuracy'])
    plt.plot(history.history['val_sparse_categorical_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper right')
    plt.show()

    # plot loss
    plt.subplot(2, 1, 2)
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper right')
    plt.show()


def plot_predictions(test_it, model, categories):
    iterator = test_it
    batch_size = iterator.batch_size
    iterator.batch_size = 1
    x_test, y_true = iterator.next()
    y_test = model.predict(x_test)
    y_labels = np.argmax(y_test, axis=-1)
    y_labels = np.where(x_test[1][:, :, :, 0] == 0, 0, y_labels)

    plt.figure(figsize=(12, 18))
    for i in range(6):
        j = 1
        plt.subplot(6, 4, 4*i + j)
        plt.imshow(x_test[0][i, :, :], cmap="gray")
        plt.axis("off")
        if i == 0:
            plt.title('Fluorescence')
        j += 1
        plt.subplot(6, 4, 4*i + j)
        plt.imshow(x_test[1][i, :, :], cmap="gray")
        plt.axis("off")
        if i == 0:
            plt.title('EDM')
        j += 1
        plt.subplot(6, 4, 4*i + j)
        plt.imshow(y_labels[i, :, :], cmap="nipy_spectral", vmin=0, vmax=3)
        plt.axis("off")
        if i == 0:
            plt.title('Predicted labels')
        j += 1
        plt.subplot(6, 4, 4*i + j)
        plt.imshow(y_true[0][i, :, :], cmap="nipy_spectral", vmin=0, vmax=3)
        plt.axis("off")
        if i == 0:
            plt.title('Ground-truth labels')

    plt.show()
    iterator.batch_size = batch_size
